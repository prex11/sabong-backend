<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Betting extends Model
{
    use HasFactory;

    protected $fillable = [
        'fight_id',
        'user_id',
        'ring_side',
        'amount',
        'reference_number',
        'mashador_bettor_name',
        'claimed',
    ];

    public function scopeFightId($query, $id) {
        return $query->where('fight_id', '=', $id, 'and');
    }

    public function scopeUserId($query, $id) {
        return $query->where('user_id', '=', $id, 'and');
    }

    public function scopeReferenceNumber($query, $ref) {
        return $query->orWhere('reference_number', 'LIKE', "%$ref%");
    }

    public function scopeUser($query, $name)
    {
        return $query->join('users', 'users.id', '=', 'bettings.user_id')
            ->orWhere('users.name', 'LIKE', "%$name%");
    }

    public function scopeFightStatus($query, $status)
    {
        return $query->join('fights', 'fights.id', '=', 'bettings.fight_id')
            ->whereIn('fights.status', $status);
    }

    public function fight()
    {
        return $this->belongsTo(Fight::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
