<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CreditHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'fight_id',
        'user_id',
        'cashier_id',
        'amount',
        'balance',
        'action_type',
    ];

    public function fight()
    {
        return $this->belongsTo(Fight::class);
    }

    public function scopeActionTypes($query, $action_types)
    {
        return $query->whereIn('credit_histories.action_type', $action_types);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cashier()
    {
        return $this->belongsTo(User::class, 'cashier_id');
    }

    public function scopeCreatedAt($query, $date)
    {
        return $query->where('created_at', 'LIKE',  "{$date}%");
    }
}
