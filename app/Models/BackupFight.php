<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BackupFight extends Model
{
    use HasFactory;

    protected $fillable = [
        'fight_id',
        'tournament_id',
        'fight_number',
        'wala_name',
        'wala_picture',
        'meron_name',
        'meron_picture',
        'total_bettings',
        'total_fighter_bettings',
        'status',
        'time_to_close',
        'time_to_finish_fight',
        'disable_timer_autoclose',
        'total_payouts',
        'total_earnings',
        'winner',
        'created_at',
        'updated_at',
    ];
}
