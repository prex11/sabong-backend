<?php

namespace App\Models;

use App\Exceptions\ConstraintException;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use App\Notifications\Auth\ResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Log;

class User extends Authenticatable
{
    use HasApiTokens, HasRoles, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'profile_image', 'credit'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Mutator for hashing the password on save
     *
     * @param string $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * A User has many items
     *
     * @return HasMany
     */
    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function bettings()
    {
        return $this->hasMany(Betting::class);
    }

    public function fighterBettings()
    {
        return $this->hasMany(FighterBetting::class);
    }

    public function cashierCreditHistory()
    {
        return $this->hasMany(CreditHistory::class, 'cashier_id', 'id');
    }

    /**
     * Send the password reset notification.
     *
     * @param string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * @param $query
     * @param $name
     * @return mixed
     */
    public function scopeName($query, $name)
    {
        return $query->where('users.name', 'LIKE', "%$name%", 'or');
    }

    /**
     * @param $query
     * @param $email
     * @return mixed
     */
    public function scopeEmail($query, $email)
    {
        return $query->where('users.email', 'LIKE', "%$email%", 'or');
    }

    /**
     * @param $query
     * @param $role
     * @return mixed
     */
    public function scopeRoles($query, $role)
    {
        return $query->WhereHas('roles', function ($query) use ($role) {
            $query->where('roles.name', 'LIKE', "%$role%");
        });
    }

    public function scopeInRoles($query, $roles)
    {
        return $query->WhereHas('roles', function ($query) use ($roles) {
            return $query->whereIn('roles.name', $roles);
        });
    }


    /**
     * @return bool|null
     * @throws ConstraintException
     */
    public function delete()
    {
        if ($this->id == auth()->id()) {
            throw new ConstraintException('You cannot delete yourself.');
        }

        return parent::delete();
    }

}
