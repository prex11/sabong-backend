<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BackupBetting extends Model
{
    use HasFactory;

    protected $fillable = [
        'betting_id',
        'fight_id',
        'user_id',
        'ring_side',
        'amount',
        'payout',
        'tong',
        'payout_status',
        'reference_number',
        'mashador_bettor_name',
        'claimed',
        'created_at',
        'updated_at'
    ];
}
