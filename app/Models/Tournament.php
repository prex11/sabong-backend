<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'start_date',
        'status',
        'tong_percentage',
        'timer',
        'date_started',
        'date_closed'
    ];

    public function fights()
    {
        return $this->hasMany(Fight::class);
    }
}
