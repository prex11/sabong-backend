<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fight extends Model
{
    use HasFactory;

    protected $fillable = [
        'tournament_id',
        'fight_number',
        'wala_name',
        'wala_picture',
        'meron_name',
        'meron_picture',
        'total_bettings',
        'total_fighter_bettings',
        'status',
        'winner',
        'total_earnings',
        'total_payouts',
        'time_to_close',
        'time_to_finish_fight',
        'timer_started',
        'disable_timer_autoclose'
    ];

    public function scopeTournamentId($query, $id)
    {
        return $query->where('tournament_id', '=', $id, 'and');
    }

    public function scopeStatus($query, $status)
    {
        return $query->whereIn('status', $status);
    }

    public function scopeActiveOnly($query)
    {
        return $query->where('status', '!=', 'finish', 'and');
    }

    public function scopeBettingsByUser($query, $id)
    {
        return $query->WhereHas('bettings', function ($query) use ($id) {
            $query->where('bettings.user_id', '=', $id);
        });
    }

    public function tournament()
    {
        return $this->belongsTo(Tournament::class);
    }

    public function bettings()
    {
        return $this->hasMany(Betting::class);
    }

    public function figherBettings()
    {
        return $this->hasMany(FigtherBetting::class);
    }
}
