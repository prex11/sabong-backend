<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FighterBetting extends Model
{
    use HasFactory;

    protected $fillable = [
        'fight_id',
        'user_id',
        'ring_side',
        'amount',
    ];

    public function fight()
    {
        return $this->belongsTo(Fight::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
