<?php

namespace App\Events;

use App\Models\Fight;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class FightUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $fight;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        if (!$id) {
            $this->fight = 0;
        } else {
            $fight = Fight::find($id);
            $this->fight = array(
                'type' => 'fights',
                'id' => $id,
                'tournament_id' => $fight->tournament_id,
                'fight_number' => $fight->fight_number,
                'wala_name' => $fight->wala_name,
                'wala_picture' => $fight->wala_picture,
                'meron_name' => $fight->meron_name,
                'meron_picture' => $fight->meron_picture,
                'total_bettings' => $fight->total_bettings,
                'total_fighter_bettings' => $fight->total_fighter_bettings,
                'winner' => $fight->winner,
                'total_earnings'  => $fight->total_earnings,
                'total_payouts'  => $fight->total_payouts,
                'wala_total' =>  $fight->bettings->where('ring_side', 'wala')->sum('amount'),
                'meron_total' =>  $fight->bettings->where('ring_side', 'meron')->sum('amount'),
                'status'  => $fight->status,
                'tournament' => $fight->tournament,
                'time_to_close' => $fight->time_to_close,
                'time_to_finish_fight' => $fight->time_to_finish_fight,
                'disable_timer_autoclose' => $fight->disable_timer_autoclose
            );
        }
        
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        $id = (!$this->fight) ? 0 : $this->fight['id'];
        return new Channel('fight-' . $id);
    }
}
