<?php

namespace App\JsonApi\V1\CreditHistories;

use Neomerx\JsonApi\Schema\SchemaProvider;

class Schema extends SchemaProvider
{

    /**
     * @var string
     */
    protected $resourceType = 'credit-histories';

    /**
     * @param \App\CreditHistory $resource
     *      the domain record being serialized.
     * @return string
     */
    public function getId($resource)
    {
        return (string) $resource->getRouteKey();
    }

    /**
     * @param \App\CreditHistory $resource
     *      the domain record being serialized.
     * @return array
     */
    public function getAttributes($resource)
    {
        return [
            'fight_id' => $resource->fight_id,
            'user_id' => $resource->user_id,
            'cashier_id' => $resource->cashier_id,
            'amount' => $resource->amount,
            'balance' => $resource->balance,
            'action_type' => $resource->action_type,
            'created_at' => optional($resource->created_at)->format('Y-m-d H:i:s'),
            'updated_at' => optional($resource->updated_at)->format('Y-m-d H:i:s'),
        ];
    }

    public function getRelationships($item, $isPrimary, array $includeRelationships)
    {
        return [
            'fight' => [
                self::SHOW_SELF => false,
                self::SHOW_RELATED => true,
                self::SHOW_DATA => isset($includeRelationships['fight']),
                self::DATA => function () use ($item) {
                    return $item->fight;
                },
            ],
            'user' => [
                self::SHOW_SELF => false,
                self::SHOW_RELATED => true,
                self::SHOW_DATA => isset($includeRelationships['user']),
                self::DATA => function () use ($item) {
                    return $item->user;
                },
            ],
            'cashier' => [
                self::SHOW_SELF => false,
                self::SHOW_RELATED => true,
                self::SHOW_DATA => isset($includeRelationships['cashier']),
                self::DATA => function () use ($item) {
                    return $item->cashier;
                },
            ],
        ];
    }
}
