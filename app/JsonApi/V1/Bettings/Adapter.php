<?php

namespace App\JsonApi\V1\Bettings;

use App\Events\ClaimBet;
use App\Events\FightUpdated;
use App\Models\Betting;
use App\Models\Fight;
use App\Models\StationBalance;
use CloudCreativity\LaravelJsonApi\Eloquent\AbstractAdapter;
use CloudCreativity\LaravelJsonApi\Pagination\StandardStrategy;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class Adapter extends AbstractAdapter
{

    /**
     * Mapping of JSON API attribute field names to model keys.
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * Mapping of JSON API filter names to model scopes.
     *
     * @var array
     */
    protected $filterScopes = [];

    /**
     * Adapter constructor.
     *
     * @param StandardStrategy $paging
     */
    public function __construct(StandardStrategy $paging)
    {
        parent::__construct(new \App\Models\Betting(), $paging);
    }

    /**
     * @param Builder $query
     * @param Collection $filters
     * @return void
     */
    protected function filter($query, Collection $filters)
    {
        $this->filterWithScopes($query, $filters);
    }

    public function saved(Betting $item) {
        if ($item->claimed) {
            $station_balance = StationBalance::where('tournament_id', $item->fight->tournament_id)->where('user_id', auth()->id())->first();
            if ($station_balance) {
                $station_balance->decrement('amount', $item->payout);
            }

            event(new ClaimBet($item->id));
        }
    }

    public function deleted(Betting $item) {

        $fight = Fight::find($item->fight_id);
        $fight->decrement('total_bettings', $item->amount);
        $fight->decrement('total_payouts', $item->amount);

        event(new FightUpdated($item->fight_id));

    }

}
