<?php

namespace App\JsonApi\V1\Bettings;

use Neomerx\JsonApi\Schema\SchemaProvider;

class Schema extends SchemaProvider
{

    /**
     * @var string
     */
    protected $resourceType = 'bettings';

    /**
     * @param \App\Betting $resource
     *      the domain record being serialized.
     * @return string
     */
    public function getId($resource)
    {
        return (string) $resource->getRouteKey();
    }

    /**
     * @param \App\Betting $resource
     *      the domain record being serialized.
     * @return array
     */
    public function getAttributes($resource)
    {
        return [
            'fight_id' => $resource->fight_id,
            'user_id' => $resource->user_id,
            'ring_side' => $resource->ring_side,
            'amount' => $resource->amount,
            'ring_side' => $resource->ring_side,
            'payout' => $resource->payout,
            'payout_status' => $resource->payout_status,
            'tong' => $resource->tong,
            'reference_number' => $resource->reference_number,
            'mashador_bettor_name' => $resource->mashador_bettor_name,
            'claimed' => $resource->claimed,
            'createdAt' => optional($resource->created_at)->format('Y-m-d H:i:s'),
            'updatedAt' => optional($resource->updatedAt)->format('Y-m-d H:i:s'),
        ];
    }

    public function getRelationships($item, $isPrimary, array $includeRelationships)
    {
        return [
            'fight' => [
                self::SHOW_SELF => false,
                self::SHOW_RELATED => true,
                self::SHOW_DATA => isset($includeRelationships['fight']),
                self::DATA => function () use ($item) {
                    return $item->fight;
                },
            ],
            'user' => [
                self::SHOW_SELF => false,
                self::SHOW_RELATED => true,
                self::SHOW_DATA => isset($includeRelationships['user']),
                self::DATA => function () use ($item) {
                    return $item->user;
                },
            ],
        ];
    }
}
