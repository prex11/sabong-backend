<?php

namespace App\JsonApi\V1\Bettings;

use CloudCreativity\LaravelJsonApi\Validation\AbstractValidators;

class Validators extends AbstractValidators
{

    /**
     * The include paths a client is allowed to request.
     *
     * @var string[]|null
     *      the allowed paths, an empty array for none allowed, or null to allow all paths.
     */
    protected $allowedIncludePaths = ['fight', 'fight.tournament', 'user'];

    /**
     * The sort field names a client is allowed send.
     *
     * @var string[]|null
     *      the allowed fields, an empty array for none allowed, or null to allow all fields.
     */
    protected $allowedSortParameters = ['created_at'];

    /**
     * The filters a client is allowed send.
     *
     * @var string[]|null
     *      the allowed filters, an empty array for none allowed, or null to allow all.
     */
    protected $allowedFilteringParameters = ['fight_id', 'user_id', 'payout_status', 'fight_status', 'reference_number', 'user'];

    /**
     * Get resource validation rules.
     *
     * @param mixed|null $record
     *      the record being updated, or null if creating a resource.
     * @param array $data
     *      the data being validated
     * @return array
     */
    protected function rules($record, array $data): array
    {
        if($record) {
            return [
                'fight_id' => 'exists:fights,id',
                // 'user_id' => 'exists:users,id',
                'ring_side' => 'required|string',
                'amount' => 'required',
            ];
        }

        return [
            'fight_id' => 'exists:fights,id',
            // 'user_id' => 'exists:users,id',
            'ring_side' => 'required|string',
            'amount' => 'required',
        ];
    }

    /**
     * Get query parameter validation rules.
     *
     * @return array
     */
    protected function queryRules(): array
    {
        return [
            //
        ];
    }

}
