<?php

namespace App\JsonApi\V1\Users;

use App\Events\UserUpdated;
use App\Models\CreditHistory;
use App\Models\User;
use App\Strategies\PaginationStrategy;
use CloudCreativity\LaravelJsonApi\Eloquent\AbstractAdapter;
use CloudCreativity\LaravelJsonApi\Eloquent\HasMany;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Neomerx\JsonApi\Contracts\Encoder\Parameters\SortParameterInterface;

class Adapter extends AbstractAdapter
{

    /**
     * Mapping of JSON API attribute field names to model keys.
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * @var string
     */
    protected $defaultSort = '-created_at';

    /**
     * Mapping of JSON API filter names to model scopes.
     *
     * @var array
     */
    protected $filterScopes = [];

    /**
     * Adapter constructor.
     *
     * @param PaginationStrategy $paging
     */
    public function __construct(PaginationStrategy $paging)
    {
        parent::__construct(new \App\Models\User(), $paging);
    }

    /**
     * @param Builder $query
     * @param Collection $filters
     * @return void
     */
    protected function filter($query, Collection $filters)
    {
        // $auth_user = auth()->user();
        // $is_worker = false;
        // foreach($auth_user->roles as $role) {
        //     if ($role->name == 'creator'){
        //         $is_worker = true;
        //         break;
        //     }
        // }
        // if ($is_worker) {           
        //     $filters['roles'] = 'member';
        // }

        unset($filters['start_date']);
        $this->filterWithScopes($query, $filters);
    }

    /**
     * Declare roles relationship
     *
     * @return HasMany
     */
    protected function roles()
    {
        return $this->hasMany();
    }

    protected function sortBy($query, SortParameterInterface $param)
    {
        $column = $this->getQualifiedSortColumn($query, $param->getField());
        $order = $param->isAscending() ? 'asc' : 'desc';

        if ($column === 'roles.name') {
            $query->leftJoin( 'model_has_roles', 'model_id', '=', 'users.id' )
                  ->leftjoin( 'roles', 'model_has_roles.role_id', '=', 'roles.id' )
                  ->groupBy('users.id')
                  ->select('users.*', DB::raw('group_concat(roles.name) as croles'));

            $query->orderBy('croles', $order)->orderBy('users.id', $order);
            return;
        }

        $query->orderBy($column, $order);
    }

    protected function created(User $item) {
        if ($item->credit) {
            CreditHistory::create([
                'cashier_id' => auth()->id(),
                'user_id' => $item->id,
                'amount' => $item->credit,
                'balance' => $item->credit,
                'action_type' => 'add-credit',
            ]);
        }
    }

    protected function saving(User $item)
    {
        
    }

    public function saved(User $item) {
        event(new UserUpdated($item->id));
    }
}
