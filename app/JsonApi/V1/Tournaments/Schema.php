<?php

namespace App\JsonApi\V1\Tournaments;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Neomerx\JsonApi\Schema\SchemaProvider;

class Schema extends SchemaProvider
{

    /**
     * @var string
     */
    protected $resourceType = 'tournaments';

    /**
     * @param \App\Tournament $resource
     *      the domain record being serialized.
     * @return string
     */
    public function getId($resource)
    {
        return (string) $resource->getRouteKey();
    }

    /**
     * @param \App\Tournament $resource
     *      the domain record being serialized.
     * @return array
     */
    public function getAttributes($resource)
    {
        return [
            'title' => $resource->title,
            'description' => $resource->description,
            'status' => $resource->status,
            'tong_percentage' => $resource->tong_percentage,
            'timer' => $resource->timer,
            'actual_total_income' =>  DB::table('fights')->where('tournament_id', $resource->id)->whereIn('winner', ['wala','meron'])->sum('total_payouts'),
            'total_income' =>  DB::table('fights')->where('tournament_id', $resource->id)->whereIn('winner', ['wala','meron'])->sum('total_earnings'),
            'start_date' => $resource->start_date,
            'date_started' => optional($resource->date_started)->format('Y-m-d H:i:s'),
            'date_closed' => optional($resource->date_closed)->format('Y-m-d H:i:s'),
            'created_at' => optional($resource->created_at)->format('Y-m-d H:i:s'),
            'updated_at' => optional($resource->updated_at)->format('Y-m-d H:i:s'),
    	]; 
    }

    public function getRelationships($item, $isPrimary, array $includeRelationships)
    {
        return [
            'fights' => [
                self::SHOW_SELF => false,
                self::SHOW_RELATED => true,
                self::SHOW_DATA => isset($includeRelationships['fights']),
                self::DATA => function () use ($item) {
                    return $item->fights;
                },
            ],
        ];
    }
}
