<?php

namespace App\JsonApi\V1\Fights;

use Neomerx\JsonApi\Schema\SchemaProvider;

class Schema extends SchemaProvider
{

    /**
     * @var string
     */
    protected $resourceType = 'fights';

    /**
     * @param \App\Fight $resource
     *      the domain record being serialized.
     * @return string
     */
    public function getId($resource)
    {
        return (string) $resource->getRouteKey();
    }

    /**
     * @param \App\Fight $resource
     *      the domain record being serialized.
     * @return array
     */
    public function getAttributes($resource)
    {
        return [
            'tournament_id' => $resource->tournament_id,
            'fight_number' => $resource->fight_number,
            'wala_name' => $resource->wala_name,
            'wala_picture' => $resource->wala_picture,
            'meron_name' => $resource->meron_name,
            'meron_picture' => $resource->meron_picture,
            'total_bettings' => $resource->total_bettings,
            'total_fighter_bettings' => $resource->total_fighter_bettings,
            'winner' => $resource->winner,
            'total_earnings'  => $resource->total_earnings,
            'total_payouts'  => ($resource->winner == 'draw') ? 0 : $resource->total_payouts,
            'wala_total' =>  $resource->bettings->where('ring_side', 'wala')->sum('amount'),
            'meron_total' =>  $resource->bettings->where('ring_side', 'meron')->sum('amount'),
            'status'  => $resource->status,
            'time_to_close'  => $resource->time_to_close,
            'time_to_finish_fight'  => $resource->time_to_finish_fight,
            'disable_timer_autoclose' => $resource->disable_timer_autoclose,
            'unclaimed_total' => '',
            'tong_total' => '',
            'created_at' => optional($resource->created_at)->format('Y-m-d H:i:s'),
            'updated_at' => optional($resource->updated_at)->format('Y-m-d H:i:s'),
        ];
    }

    public function getRelationships($item, $isPrimary, array $includeRelationships)
    {
        return [
            'tournament' => [
                self::SHOW_SELF => false,
                self::SHOW_RELATED => true,
                self::SHOW_DATA => isset($includeRelationships['tournament']),
                self::DATA => function () use ($item) {
                    return $item->tournament;
                },
            ],
        ];
    }
}

