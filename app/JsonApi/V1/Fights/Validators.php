<?php

namespace App\JsonApi\V1\Fights;

use CloudCreativity\LaravelJsonApi\Validation\AbstractValidators;

class Validators extends AbstractValidators
{

    /**
     * The include paths a client is allowed to request.
     *
     * @var string[]|null
     *      the allowed paths, an empty array for none allowed, or null to allow all paths.
     */
    protected $allowedIncludePaths = ['tournament'];

    /**
     * The sort field names a client is allowed send.
     *
     * @var string[]|null
     *      the allowed fields, an empty array for none allowed, or null to allow all fields.
     */
    protected $allowedSortParameters = ['fight_number',
    'wala_name',
    'wala_picture',
    'meron_name',
    'meron_picture',
    'total_bettings',
    'total_fighter_bettings',
    'status', 'created_at'];

    /**
     * The filters a client is allowed send.
     *
     * @var string[]|null
     *      the allowed filters, an empty array for none allowed, or null to allow all.
     */
    protected $allowedFilteringParameters = ['tournament_id', 'active_only', 'status', 'bettings_by_user'];

    /**
     * Get resource validation rules.
     *
     * @param mixed|null $record
     *      the record being updated, or null if creating a resource.
     * @param array $data
     *      the data being validated
     * @return array
     */
    protected function rules($record, array $data): array
    {
        if($record) {
            return [
                'tournament_id' => 'exists:tournaments,id',
                'wala_name' => 'sometimes|string',
                'meron_name' => 'sometimes|string',
            ];
        }
        
        return [
            'tournament_id' => 'exists:tournaments,id',
            'wala_name' => 'required|string',
            'meron_name' => 'required|string',
        ];
    }

    /**
     * Get query parameter validation rules.
     *
     * @return array
     */
    protected function queryRules(): array
    {
        return [
            //
        ];
    }

}
