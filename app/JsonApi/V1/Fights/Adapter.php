<?php

namespace App\JsonApi\V1\Fights;

use App\Events\BroadcastFights;
use App\Events\FightUpdated;
use App\Exceptions\ConstraintException;
use App\Models\BackupBetting;
use App\Models\BackupFight;
use App\Models\Fight;
use Carbon\Carbon;
use CloudCreativity\LaravelJsonApi\Eloquent\AbstractAdapter;
use CloudCreativity\LaravelJsonApi\Pagination\StandardStrategy;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class Adapter extends AbstractAdapter
{

    private $creating = false;

    /**
     * Mapping of JSON API attribute field names to model keys.
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * Mapping of JSON API filter names to model scopes.
     *
     * @var array
     */
    protected $filterScopes = [];

    /**
     * Adapter constructor.
     *
     * @param StandardStrategy $paging
     */
    public function __construct(StandardStrategy $paging)
    {
        parent::__construct(new \App\Models\Fight(), $paging);
    }

    /**
     * @param Builder $query
     * @param Collection $filters
     * @return void
     */
    protected function filter($query, Collection $filters)
    {
        $this->filterWithScopes($query, $filters);
    }

    protected function saving(Fight $item)
    {
        
        if ($item->status == 'open') {

            $tournament_timer = $item->tournament->timer;
            $time_to_close = Carbon::now()->addMinutes($tournament_timer);

            $item->time_to_close = $time_to_close;
        }

        if ($item->timer_started) {
            
            $time_to_finish_fight = Carbon::now()->addMinutes(10);
            $item->time_to_finish_fight = ($item->timer_started == 'on') ? $time_to_finish_fight : null;
            unset($item->timer_started);
        }

    }

    protected function creating(Fight $item) {

        $fight = Fight::whereIn('status', ['open', 'on-going', 'close'])
            // ->where('tournament_id', $item->tournament_id)
            ->first();

        if ($fight) {
            throw new ConstraintException('Active fight found.');
        }

        $this->creating = true;
    }

    protected function saved(Fight $item)
    {
        if (!isset($item->fight_number)) {
            $fight_number = $item->tournament->fights()->count();
            $item->fight_number = $fight_number;
            $item->save();
        }
	
	    if ($item->status == 'on-going') {
            $wala_total =  $item->bettings->where('ring_side', 'wala')->sum('amount');
            $meron_total =  $item->bettings->where('ring_side', 'meron')->sum('amount');
            $total = $wala_total + $meron_total;

            $item->total_bettings = $total;
            $item->total_payouts = $total;
            $item->save();

            $backup_fight = BackupFight::where('fight_id', $item->id)->first();
            if (!$backup_fight) {
                BackupFight::create([
                    'fight_id' => $item->id,
                    'tournament_id' => $item->tournament_id,
                    'fight_number' => $item->fight_number,
                    'wala_name' => $item->wala_name,
                    'wala_picture' => $item->wala_picture,
                    'meron_name' => $item->meron_name,
                    'meron_picture' => $item->meron_picture,
                    'total_bettings' => $item->total_bettings,
                    'total_fighter_bettings' => $item->total_fighter_bettings,
                    'status' => $item->status,
                    'time_to_close' => $item->time_to_close,
                    'time_to_finish_fight' => $item->time_to_finish_fight,
                    'disable_timer_autoclose' => $item->disable_timer_autoclose,
                    'total_payouts' => $item->total_payouts,
                    'total_earnings' => $item->total_earnings,
                    'winner' => $item->winner,
                    'created_at' => $item->created_at,
                    'updated_at' => $item->updated_at
                ]);

                foreach($item->bettings as $betting) {
                    BackupBetting::create([
                        'betting_id' => $betting->id,
                        'fight_id' => $betting->fight_id,
                        'user_id' => $betting->user_id,
                        'ring_side' => $betting->ring_side,
                        'amount' => $betting->amount,
                        'payout' => $betting->payout,
                        'tong' => $betting->tong,
                        'payout_status' => $betting->payout_status,
                        'reference_number' => $betting->reference_number,
                        'mashador_bettor_name' => $betting->mashador_bettor_name,
                        'claimed' => $betting->claimed,
                        'created_at' => $betting->created_at,
                        'updated_at' => $betting->updated_at
                    ]);
                }
            }
        }

        if ($this->creating)
            event(new FightUpdated(0));
        else
            event(new FightUpdated($item->id));
        
        event(new BroadcastFights());
    }

    



}
