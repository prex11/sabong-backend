<?php

namespace App\Http\Controllers\Api\V1;

use App\Events\BroadcastFights;
use App\Events\FightUpdated;
use App\Events\UserUpdated;
use App\Models\Betting;
use App\Models\CreditHistory;
use App\Models\Fight;
use App\Models\StationBalance;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use GuzzleHttp\Exception\ClientException;
use CloudCreativity\LaravelJsonApi\Document\Error\Error;
use CloudCreativity\LaravelJsonApi\Http\Controllers\JsonApiController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class BettingController extends JsonApiController
{

    public function placeBet(Request $request)
    {
        try {

            $data = $request['data']['attributes'];

            $fight = Fight::find( $data['fight_id']);

            if (!$fight) {
                throw new \Exception('Fight not found.');
            }

            $user = User::find(auth()->id());
            if ($user->credit < $data['amount']) {
                event(new UserUpdated(auth()->id()));
                return response()->json(['message' => 'Not enough credit.'], 400);
            }

            if ($fight->status != "open") {
                return response()->json(['message' => 'fight closed.'], 400);
            }

            DB::transaction(function() use ($data, $fight) {

                User::find(auth()->id())->decrement('credit', $data['amount']);

                $betting = Betting::where('fight_id', $fight->id)
                    ->where('user_id', auth()->id())
                    ->where('ring_side', $data['ring_side'])
                    ->first();

                if ($betting) {
                    $betting->increment('amount', $data['amount']);
                    $betting->payout_status = "";
                    $betting->payout = 0;
                    $betting->save();
                } else {
                    $betting = Betting::create([
                        'fight_id' => $fight->id,
                        'user_id' => auth()->id(),
                        'ring_side' => $data['ring_side'],
                        'amount' => $data['amount'],
                        'payout_status' => "",
                        'payout' => 0
                    ]);
                }

                $fight->increment('total_bettings', $data['amount']);
                $fight->total_payouts = $fight->bettings->sum('amount');
                $fight->save();

                $dc_user = User::find(auth()->id());
                CreditHistory::create([
                    'fight_id' => $fight->id,
                    'user_id' => auth()->id(),
                    'amount' => $data['amount'],
                    'balance' => $dc_user->credit,
                    'action_type' => 'place-bet',
                ]);
            });


            event(new UserUpdated(auth()->id()));
            event(new FightUpdated($fight->id));
            event(new BroadcastFights());

            $my_bets = Betting::where('user_id', auth()->id())
                ->where('fight_id', $fight->id)
                ->get();

            return response()->json($my_bets, 200);
            
        } catch (ClientException $e) {

            $errors = json_decode($e->getResponse()->getBody()->getContents(), true)['errors'];
            $errors = collect($errors)->map(function ($error) { return Error::fromArray($error); });

            return $this->reply()->errors($errors);
        }
    }

    public function placeBetMashador(Request $request)
    {
        try {

            $data = $request['data']['attributes'];

            $fight = Fight::find( $data['fight_id']);

            if (!$fight) {
                throw new \Exception('Fight not found.');
            }

            $user = User::find(auth()->id());
            if ($user->credit < $data['amount']) {
                event(new UserUpdated(auth()->id()));
                return response()->json(['message' => 'Not enough credit.'], 400);
            }

            if ($fight->status != "open") {
                return response()->json(['message' => 'fight closed.'], 400);
            }

            $reference_number = $fight->fight_number . Str::random(6) . $fight->id;
            DB::transaction(function() use ($data, $fight, $reference_number) {

                User::find(auth()->id())->decrement('credit', $data['amount']);

                $betting = Betting::create([
                    'fight_id' => $fight->id,
                    'user_id' => auth()->id(),
                    'ring_side' => $data['ring_side'],
                    'amount' => $data['amount'],
                    'reference_number' => $reference_number,
                    'mashador_bettor_name' => $data['mashador_bettor_name'],
                    'payout_status' => "",
                    'payout' => 0
                ]);

                $fight->increment('total_bettings', $data['amount']);
                $fight->total_payouts = $fight->bettings->sum('amount');
                $fight->save();

                $dc_user = User::find(auth()->id());
                CreditHistory::create([
                    'fight_id' => $fight->id,
                    'user_id' => auth()->id(),
                    'amount' => $data['amount'],
                    'balance' => $dc_user->credit,
                    'action_type' => 'place-bet',
                ]);

                $station_balance = StationBalance::where('tournament_id', $fight->tournament_id)->where('user_id', auth()->id())->first();
                if ($station_balance) {
                    $station_balance->increment('amount', $data['amount']);
                } else {
                    StationBalance::create([
                        'tournament_id' => $fight->tournament_id,
                        'user_id' => auth()->id(),
                        'amount' => $data['amount']
                    ]);
                }
            });


            event(new UserUpdated(auth()->id()));
            event(new FightUpdated($fight->id));
            event(new BroadcastFights());

            $my_bet = Betting::where('user_id', auth()->id())
                ->where('fight_id', $fight->id)
                ->where('reference_number', $reference_number)
                ->first();

            return response()->json($my_bet, 200);
            
        } catch (ClientException $e) {

            $errors = json_decode($e->getResponse()->getBody()->getContents(), true)['errors'];
            $errors = collect($errors)->map(function ($error) { return Error::fromArray($error); });

            return $this->reply()->errors($errors);
        }
    }

    /**
     * Parse headers to collapse internal arrays
     * TODO: move to helpers
     *
     * @param array $headers
     * @return array
     */
    protected function parseHeaders($headers)
    {
        return collect($headers)->map(function ($item) {
            return $item[0];
        })->toArray();
    }
}
