<?php

namespace App\Http\Controllers\Api\V1;

use App\Events\UserUpdated;
use App\Models\Betting;
use App\Models\CreditHistory;
use App\Models\StationBalance;
use App\Models\Tournament;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use GuzzleHttp\Exception\ClientException;
use CloudCreativity\LaravelJsonApi\Document\Error\Error;
use CloudCreativity\LaravelJsonApi\Http\Controllers\JsonApiController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends JsonApiController
{
    public function addCredit(Request $request, $id)
    {
        try {

            $data = $request['data']['attributes'];
            $amount = $data['amount'];
            $user = User::find($id);
            $auth_user = User::find(auth()->id());

            if ($user) {
                
                
                $user->increment('credit', $amount);
                
                $role_names = [];
                foreach ($auth_user->roles as $role) {
                    array_push($role_names, $role->name);
                }

                if (in_array('creator', $role_names) ){
                    $auth_user->increment('credit', $amount);
                } 

                CreditHistory::create([
                    'cashier_id' => $auth_user->id,
                    'user_id' => $user->id,
                    'amount' => $amount,
                    'balance' => $user->credit,
                    'action_type' => 'add-credit',
                ]);

                event(new UserUpdated($auth_user->id));
                event(new UserUpdated($user->id));
            }

            return response()->json($user, 200);
        
        } catch (ClientException $e) {

            $errors = json_decode($e->getResponse()->getBody()->getContents(), true)['errors'];
            $errors = collect($errors)->map(function ($error) { return Error::fromArray($error); });

            return $this->reply()->errors($errors);
        }
    }

    public function cashOut(Request $request, $id)
    {
        try {

            $data = $request['data']['attributes'];
            $amount = $data['amount'];
            $user = User::find($id);
            $auth_user = User::find(auth()->id());

            if ($user) {
                
                if ($user->credit < $amount) {
                    event(new UserUpdated($user->id));
                    return response()->json(['message' => 'Not enough credit.'], 400);
                }
                
                $user->decrement('credit', $amount);
                
                $role_names = [];
                foreach ($auth_user->roles as $role) {
                    array_push($role_names, $role->name);
                }

                if (in_array('creator', $role_names) ){
                    $auth_user->decrement('credit', $amount);
                } 

                CreditHistory::create([
                    'cashier_id' => $auth_user->id,
                    'user_id' => $user->id,
                    'amount' => $amount,
                    'balance' => $user->credit,
                    'action_type' => 'cash-out',
                ]);

                event(new UserUpdated($auth_user->id));
                event(new UserUpdated($user->id));
            }

            return response()->json($user, 200);
        
        } catch (ClientException $e) {

            $errors = json_decode($e->getResponse()->getBody()->getContents(), true)['errors'];
            $errors = collect($errors)->map(function ($error) { return Error::fromArray($error); });

            return $this->reply()->errors($errors);
        }
    }

    public function getStationsIncome(Request $request)
    {
        $http = new Client(['verify' => false]);

        $headers = $this->parseHeaders($request->header());

        $headers = [
            'Accept' => 'application/vnd.api+json',
            'Authorization' => $headers['authorization']
        ];

        $input = $request->json()->all();
        $input['data']['type'] = 'users';

        $query = $request->query();
        $start_date = $query['filter']['start_date'];
        $tournament_id = 0;
        if (isset($query['filter']['tournament_id'])) {
            $tournament_id = $query['filter']['tournament_id'];
        }

        unset($query['filter']['start_date']);
        unset($query['filter']['tournament_id']);

        $data = [
            'headers' => $headers,
            'query' => $query
        ];

        try {
            $response = $http->get(route('api:v1:users.index'), $data);
            $responseBody = json_decode((string)$response->getBody(), true);

            $body_now = array();
            foreach($responseBody['data'] as $data) {
                $user = User::find($data['id']);
                
                $tong = 0;
                $balance = 0;
                $unclaimed = 0;
                if ($tournament_id) {
                    $tong_total = $user->bettings()
                        ->select('fights.tournament_id as tournament_id', DB::raw('SUM(bettings.tong) as tong'))
                        ->join('fights', 'bettings.fight_id', '=', 'fights.id')
                        ->where('bettings.user_id', $user->id)
                        ->where('tournament_id', $tournament_id)
                        ->groupBy('tournament_id')
                        ->get();
                    $tong = count($tong_total) ? $tong_total[0]->tong : 0;

                    $station_balance = StationBalance::where('tournament_id', $tournament_id)->where('user_id', $user->id)->first();
                    if ($station_balance) {
                        $balance = $station_balance->amount;
                    }

                    $unclaimed_total = $user->bettings()
                        ->select('fights.tournament_id as tournament_id', DB::raw('SUM(bettings.payout) as unclaimed'))
                        ->join('fights', 'bettings.fight_id', '=', 'fights.id')
                        ->where('bettings.user_id', $user->id)
                        ->where('bettings.claimed', 0)
                        ->where('tournament_id', $tournament_id)
                        ->groupBy('tournament_id')
                        ->get();
                    $unclaimed = count($unclaimed_total) ? $unclaimed_total[0]->unclaimed : 0;
                    
                } else {
                    $tong_total = $user->bettings()
                        ->where('user_id', $user->id)
                        ->sum('tong');
                    $tong = $tong_total;
                }

                $data['attributes']['tong_total'] = $tong;
                $data['attributes']['balance'] = $balance;
                $data['attributes']['unclaimed'] = $unclaimed;
                array_push($body_now, $data);
            }

            $responseBody['data'] = $body_now;

            $responseStatus = $response->getStatusCode();
            $responseHeaders = $this->parseHeaders($response->getHeaders());

            unset($responseHeaders['Transfer-Encoding']);

            return response()->json($responseBody, $responseStatus)->withHeaders($responseHeaders);
        } catch (ClientException $e) {
            $errors = json_decode($e->getResponse()->getBody()->getContents(), true)['errors'];
            $errors = collect($errors)->map(function ($error) { return Error::fromArray($error); });

            return $this->reply()->errors($errors);
        }
    }

    public function transferCreditToUser(Request $request)
    {
        try {

            $data = $request['data']['attributes'];
            $amount = $data['amount'];
            $from_user = User::find(auth()->id());
            $to_user = User::find($data['user_id']);

            if ($from_user->credit < $amount) {
                return response()->json(['message' => 'Not enough credit.'], 400);
            }

            DB::transaction(function() use ($amount, $from_user, $to_user) {
                $from_user->decrement('credit', $amount);
                $to_user->increment('credit', $amount);

                CreditHistory::create([
                    'cashier_id' => auth()->id(),
                    'user_id' => $to_user->id,
                    'amount' => $amount,
                    'balance' => $from_user->credit,
                    'action_type' => 'trasnfer-credit',
                ]);
            });

            event(new UserUpdated(auth()->id()));
            event(new UserUpdated($to_user->id));

        } catch (ClientException $e) {

            $errors = json_decode($e->getResponse()->getBody()->getContents(), true)['errors'];
            $errors = collect($errors)->map(function ($error) { return Error::fromArray($error); });

            return $this->reply()->errors($errors);
        }
    }

    public function addRevolvingFund(Request $request, $id)
    {
        try {

            $data = $request['data']['attributes'];

            $user = User::find($id);
            if ($user) {
                $amount = $data['amount'];
                $tournament_id = $data['tournament_id'];

                $station_balance = StationBalance::where('tournament_id', $tournament_id)->where('user_id', $id)->first();
                if ($station_balance) {
                    $station_balance->increment('amount', $amount);
                } else {
                    StationBalance::create([
                        'tournament_id' => $tournament_id,
                        'user_id' => $id,
                        'amount' => $amount
                    ]);
                }

                event(new UserUpdated($id));
            }

            return response()->json($user, 200);
        
        } catch (ClientException $e) {

            $errors = json_decode($e->getResponse()->getBody()->getContents(), true)['errors'];
            $errors = collect($errors)->map(function ($error) { return Error::fromArray($error); });

            return $this->reply()->errors($errors);
        }
    }

    /**
     * Parse headers to collapse internal arrays
     * TODO: move to helpers
     *
     * @param array $headers
     * @return array
     */
    protected function parseHeaders($headers)
    {
        return collect($headers)->map(function ($item) {
            return $item[0];
        })->toArray();
    }
}
