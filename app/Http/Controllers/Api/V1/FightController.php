<?php

namespace App\Http\Controllers\Api\V1;

use App\Events\BroadcastFights;
use App\Events\FightUpdated;
use App\Events\UserUpdated;
use App\Models\BackupBetting;
use App\Models\BackupFight;
use App\Models\CreditHistory;
use App\Models\Fight;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use GuzzleHttp\Exception\ClientException;
use CloudCreativity\LaravelJsonApi\Document\Error\Error;
use CloudCreativity\LaravelJsonApi\Http\Controllers\JsonApiController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FightController extends JsonApiController
{
    private $tong_earnings = 0;

    public function getFightsBettedByUser(Request $request)
    {
        $http = new Client(['verify' => false]);

        $headers = $this->parseHeaders($request->header());

        $headers = [
            'Accept' => 'application/vnd.api+json',
            'Authorization' => $headers['authorization']
        ];

        $input = $request->json()->all();
        $input['data']['type'] = 'fights';

        $query = $request->query();
        $bettings_by_user = $query['filter']['bettings_by_user'];

        $data = [
            'headers' => $headers,
            'query' => $request->query()
        ];

        try {
            $response = $http->get(route('api:v1:fights.index'), $data);
            $responseBody = json_decode((string)$response->getBody(), true);

            $body_now = array();
            foreach($responseBody['data'] as $data) {
                $fight = Fight::find($data['id']);
                $tong_total = $fight->bettings()
                    ->where('user_id', $bettings_by_user)
                    ->sum('tong');

                $unclaimed_total = $fight->bettings()
                    ->where('user_id', $bettings_by_user) 
                    ->where('claimed', 0)
                    ->sum('payout');
                
                $data['attributes']['unclaimed_total'] = $unclaimed_total;
                $data['attributes']['tong_total'] = $tong_total;
                array_push($body_now, $data);
            }

            $responseBody['data'] = $body_now;

            $responseStatus = $response->getStatusCode();
            $responseHeaders = $this->parseHeaders($response->getHeaders());

            unset($responseHeaders['Transfer-Encoding']);

            return response()->json($responseBody, $responseStatus)->withHeaders($responseHeaders);
        } catch (ClientException $e) {
            $errors = json_decode($e->getResponse()->getBody()->getContents(), true)['errors'];
            $errors = collect($errors)->map(function ($error) { return Error::fromArray($error); });

            return $this->reply()->errors($errors);
        }
    }

    public function getCurrentFight(Request $request)
    {
        $fight = Fight::whereIn('status', ['open', 'on-going', 'close'])->first();

        if (!$fight)
        {
            return response()->json(['message' => 'no on-going fight.'], 200);
        }

        $http = new Client(['verify' => false]);

        $headers = $this->parseHeaders($request->header());

        $headers = [
            'Accept' => 'application/vnd.api+json',
            'Authorization' => $headers['authorization']
        ];

        $input = $request->json()->all();
        $input['data']['id'] = (string)$fight->id;
        $input['data']['type'] = 'fights';

        $data = [
            'headers' => $headers,
            'query' => $request->query()
        ];

        try {
            $response = $http->get(route('api:v1:fights.read', ['record' => $fight->id]), $data);

            $responseBody = json_decode((string)$response->getBody(), true);
            $responseStatus = $response->getStatusCode();
            $responseHeaders = $this->parseHeaders($response->getHeaders());

            unset($responseHeaders['Transfer-Encoding']);

            return response()->json($responseBody, $responseStatus)->withHeaders($responseHeaders);
        } catch (ClientException $e) {
            $errors = json_decode($e->getResponse()->getBody()->getContents(), true)['errors'];
            $errors = collect($errors)->map(function ($error) { return Error::fromArray($error); });

            return $this->reply()->errors($errors);
        }
    }

    public function updateStatus(Request $request, $id)
    {
        $data = $request['data']['attributes'];
        $status = $data['status'];

        if (in_array($status, ['open', 'on-going']))
        {
            $fight = Fight::whereIn('status', ['open', 'on-going', 'close'])->where('id', '!=', $id)->first();
            if ($fight)
            {
                return response()->json(['message' => 'on-going fight found.'], 400);
            }
        }

        $fight = Fight::find($id);
        if((!$fight->bettings->where('ring_side', 'meron')->count() || !$fight->bettings->where('ring_side', 'wala')->count()) && $status == 'on-going') {
            return response()->json(['message' => 'No bets found.'], 400);
        }

        $new_fight = (($fight->status == 'pending' &&  $status == 'open') || $fight->status == 'cancelled' &&  $status == 'open') ? true : false;

        $http = new Client(['verify' => false]);

        $headers = $this->parseHeaders($request->header());

        $input = $request->json()->all();

        $input['data']['id'] = $id;
        $input['data']['type'] = 'fights';

        $data = [
            'headers' => $headers,
            'json' => $input,
            'query' => $request->query()
        ];

        try {
            $response = $http->patch(route('api:v1:fights.update', ['record' => $id]), $data);
            if($new_fight)
                event(new FightUpdated(0));

        } catch (ClientException $e) {

            $errors = json_decode($e->getResponse()->getBody()->getContents(), true)['errors'];
            $errors = collect($errors)->map(function ($error) { return Error::fromArray($error); });

            return $this->reply()->errors($errors);
        }

        $responseBody = json_decode((string)$response->getBody(), true);
        $responseStatus = $response->getStatusCode();
        $responseHeaders = $this->parseHeaders($response->getHeaders());

        unset($responseHeaders['Transfer-Encoding']);

        return response()->json($responseBody, $responseStatus)->withHeaders($responseHeaders);
        
    }

    public function startTimerToFinishFight(Request $request, $id)
    {
        $data = $request['data']['attributes'];
        $status = $data['status'];

        if ($status != 'on-going') {
            return response()->json(['message' => 'Not on-going fight.'], 400);
        }

        $http = new Client(['verify' => false]);

        $headers = $this->parseHeaders($request->header());

        $input = $request->json()->all();

        $input['data']['id'] = $id;
        $input['data']['type'] = 'fights';

        $data = [
            'headers' => $headers,
            'json' => $input,
            'query' => $request->query()
        ];

        try {
            $response = $http->patch(route('api:v1:fights.update', ['record' => $id]), $data);
            // event(new FightUpdated(0));

        } catch (ClientException $e) {

            $errors = json_decode($e->getResponse()->getBody()->getContents(), true)['errors'];
            $errors = collect($errors)->map(function ($error) { return Error::fromArray($error); });

            return $this->reply()->errors($errors);
        }

        $responseBody = json_decode((string)$response->getBody(), true);
        $responseStatus = $response->getStatusCode();
        $responseHeaders = $this->parseHeaders($response->getHeaders());

        unset($responseHeaders['Transfer-Encoding']);

        return response()->json($responseBody, $responseStatus)->withHeaders($responseHeaders);
        
    }

    public function finishMatch(Request $request, $id)
    {
        try {

            $data = $request['data']['attributes'];
            $winner = $data['winner'];
            $fight = Fight::find($id);

            if (!$fight) {
                throw new \Exception('Fight not found.');
            }

            if ($winner == 'draw') {
                $bettings = $fight->bettings;
            } else {
                $bettings = $fight->bettings->where('ring_side', $winner);
            }

            $total_payouts = $fight->total_payouts;
            $tong_percentage = $fight->tournament->tong_percentage / 100;

            $fight_wala_total = $fight->bettings->where('ring_side', 'wala')->sum('amount');
            $fight_meron_total = $fight->bettings->where('ring_side', 'meron')->sum('amount');

            $my_side_total = ($winner == 'meron') ? $fight_meron_total : $fight_wala_total;

            $opposite_side_total = ($winner == 'meron') ? $fight_wala_total : $fight_meron_total;
            $opposite_side_total_tong = $opposite_side_total * $tong_percentage;
            $opposite_side_total_tong = round($opposite_side_total_tong);
            $opposite_side_total = $opposite_side_total - $opposite_side_total_tong;
            
            foreach($bettings as $bet) {
                
                try {
                
                    DB::transaction(function() use ($fight, $bet, $winner, $my_side_total, $opposite_side_total, $tong_percentage, $opposite_side_total_tong) {

                        if ($winner == 'draw') {
                            $this->returnUserBet($bet, 'payout');
                            $fight->decrement('total_payouts', $bet->amount);
                        } else {
                            // start calculate payouts
                            $my_percentage_of_total = $bet->amount / $my_side_total;
                            $you_win = $opposite_side_total * $my_percentage_of_total;
                            $you_win = (int)$you_win;

                            $my_side_tong = $bet->amount * $tong_percentage;
                            $my_side_tong = round($my_side_tong);
                            $my_winnings_tong = $opposite_side_total_tong * $my_percentage_of_total;
                            $my_winnings_tong = round($my_winnings_tong);

                            $my_amount = $bet->amount - $my_side_tong;

                            $payout = $my_amount + $you_win;
                            // end calculate payouts

                            $bet->increment('payout', $payout);
                            $bet->user->increment('credit', $payout);
                            $bet->tong = ($my_side_tong + $my_winnings_tong);
                            $bet->payout_status = 'success';
                            $bet->save();

                            $fight->decrement('total_payouts', $payout);
                            $tong_earnings = $my_side_tong + $my_winnings_tong;
                            $fight->increment('total_earnings', $tong_earnings);

                            $dc_user = User::find($bet->user->id);
                            CreditHistory::create([
                                'fight_id' => $fight->id,
                                'user_id' => $bet->user->id,
                                'amount' => $payout,
                                'balance' => $dc_user->credit,
                                'action_type' => 'payout',
                            ]);
                        }
                        
                    });

                    event(new UserUpdated($bet->user->id));

                } catch (\Throwable $th) {
                    $bet->payout_status = 'fail';
                    $bet->save();
                }
            }

            $fight->winner = $winner;
            $fight->status = 'finish';
            $fight->save();

            event(new FightUpdated($fight->id));
            event(new BroadcastFights());

            return response()->json('payout successfull', 200);
            
        } catch (ClientException $e) {

            $errors = json_decode($e->getResponse()->getBody()->getContents(), true)['errors'];
            $errors = collect($errors)->map(function ($error) { return Error::fromArray($error); });

            return $this->reply()->errors($errors);
        }
    }

    private function returnUserBet($bet, $type = null) {
        try {
                
            DB::transaction(function() use ($bet, $type) {
                $bet->user->increment('credit', $bet->amount);
                $bet->payout = $bet->amount;
                $bet->amount = 0;
                $bet->payout_status = 'refunded';
                $bet->save();

                $dc_user = User::find($bet->user->id);
                CreditHistory::create([
                    'fight_id' => $bet->fight_id,
                    'user_id' => $bet->user->id,
                    'amount' => $bet->payout,
                    'balance' => $dc_user->credit,
                    'action_type' => $type,
                ]);

                event(new UserUpdated($bet->user->id));
            });
            
            return $bet;
        } catch (\Throwable $th) {
            $bet->payout_status = 'fail refund';
            $bet->save();
        }
    }

    public function cancelMatch(Request $request, $id)
    {
        try {

            $data = $request['data']['attributes'];
            $fight = Fight::find($id);

            if (!$fight) {
                throw new \Exception('Fight not found.');
            }

            $bettings = $fight->bettings;

            foreach($bettings as $bet) {
                $this->returnUserBet($bet, 'refund');
            }

            $fight->total_bettings = 0;
            $fight->total_payouts = 0;
            $fight->status = 'cancelled';
            $fight->save();

            event(new FightUpdated($fight->id));
            event(new BroadcastFights());

            return response()->json('cancellation successfull', 200);
            
        } catch (ClientException $e) {

            $errors = json_decode($e->getResponse()->getBody()->getContents(), true)['errors'];
            $errors = collect($errors)->map(function ($error) { return Error::fromArray($error); });

            return $this->reply()->errors($errors);
        }
    }

    public function undoFinishMatch(Request $request, $id) {
        try {

            $fight = Fight::find($id);
            foreach($fight->bettings as $betting) {
                DB::transaction(function() use ($fight, $betting) {
                    $betting->user->decrement('credit', $betting->payout);

                    $backup_betting = BackupBetting::where('betting_id', $betting->id)->first();
                    if ($backup_betting) {
                        $betting->fight_id = $backup_betting->fight_id;
                        $betting->user_id = $backup_betting->user_id;
                        $betting->ring_side = $backup_betting->ring_side;
                        $betting->amount = $backup_betting->amount;
                        $betting->payout = $backup_betting->payout;
                        $betting->tong = $backup_betting->tong;
                        $betting->payout_status = $backup_betting->payout_status;
                        $betting->reference_number = $backup_betting->reference_number;
                        $betting->mashador_bettor_name = $backup_betting->mashador_bettor_name;
                        $betting->claimed = $backup_betting->claimed;
                        $betting->created_at = $backup_betting->created_at;
                        $betting->updated_at = $backup_betting->updated_at;
                        $betting->save();

                        event(new UserUpdated($betting->user->id));
                    }

                    $backup_fight = BackupFight::where('fight_id', $fight->id)->first();
                    if ($backup_fight) {
                        $fight->tournament_id = $backup_fight->tournament_id;
                        $fight->fight_number = $backup_fight->fight_number;
                        $fight->wala_name = $backup_fight->wala_name;
                        $fight->wala_picture = $backup_fight->wala_picture;
                        $fight->meron_name = $backup_fight->meron_name;
                        $fight->meron_picture = $backup_fight->meron_picture;
                        $fight->total_bettings = $backup_fight->total_bettings;
                        $fight->total_fighter_bettings = $backup_fight->total_fighter_bettings;
                        $fight->status = $backup_fight->status;
                        $fight->time_to_close = $backup_fight->time_to_close;
                        $fight->time_to_finish_fight = $backup_fight->time_to_finish_fight;
                        $fight->disable_timer_autoclose = $backup_fight->disable_timer_autoclose;
                        $fight->total_payouts = $backup_fight->total_payouts;
                        $fight->total_earnings = $backup_fight->total_earnings;
                        $fight->winner = $backup_fight->winner;
                        $fight->created_at = $backup_fight->created_at;
                        $fight->updated_at = $backup_fight->updated_at;
                        $fight->save();

                        event(new FightUpdated(0));
                        event(new FightUpdated($fight->id));
                        event(new BroadcastFights());
                    }
                    
                });

                DB::table('credit_histories')->where('fight_id', $fight->id)
                    ->where('action_type', 'payout')
                    ->delete();
            }
            

        } catch (ClientException $e) {

            $errors = json_decode($e->getResponse()->getBody()->getContents(), true)['errors'];
            $errors = collect($errors)->map(function ($error) { return Error::fromArray($error); });

            return $this->reply()->errors($errors);
        }
    }

    /**
     * Parse headers to collapse internal arrays
     * TODO: move to helpers
     *
     * @param array $headers
     * @return array
     */
    protected function parseHeaders($headers)
    {
        return collect($headers)->map(function ($item) {
            return $item[0];
        })->toArray();
    }
}
