<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('permissions')->truncate();
        DB::table('role_has_permissions')->truncate();
        DB::table('model_has_permissions')->truncate();
        DB::table('model_has_roles')->truncate();
        DB::table('roles')->truncate();
        Schema::enableForeignKeyConstraints();

        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        // User permissions
        Permission::create(['name' => 'view users']);
        Permission::create(['name' => 'create users']);
        Permission::create(['name' => 'edit users']);
        Permission::create(['name' => 'delete users']);
        // Role permissions
        Permission::create(['name' => 'view roles']);
        Permission::create(['name' => 'create roles']);
        Permission::create(['name' => 'edit roles']);
        Permission::create(['name' => 'delete roles']);
        // Permissions permissions
        Permission::create(['name' => 'view permissions']);
        // Tournament permissions
        Permission::create(['name' => 'view tournaments']);
        Permission::create(['name' => 'create tournaments']);
        Permission::create(['name' => 'edit tournaments']);
        Permission::create(['name' => 'delete tournaments']);
        // Fights permissions
        Permission::create(['name' => 'view fights']);
        Permission::create(['name' => 'create fights']);
        Permission::create(['name' => 'edit fights']);
        Permission::create(['name' => 'delete fights']);
        // Bettings permissions
        Permission::create(['name' => 'view bettings']);
        Permission::create(['name' => 'create bettings']);
        Permission::create(['name' => 'edit bettings']);
        Permission::create(['name' => 'delete bettings']);


        // create roles and assign created permissions

        // this can be done as separate statements
        // $role = Role::create(['name' => 'writer']);
        // $role->givePermissionTo('edit articles');

        // or may be done by chaining
        // $role = Role::create(['name' => 'moderator'])->givePermissionTo(['publish articles', 'unpublish articles']);

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo(Permission::all());

        $role = Role::create(['name' => 'mashador']);
        $role->givePermissionTo([
            'view users',
            'create users',
            'edit users',
            'delete users',

            'view roles',
            'create roles',
            'edit roles',
            'delete roles',

            'view fights',
            'create fights',
            'edit fights',
            'delete fights',

            'view bettings',
            'create bettings',
            'edit bettings',
            'delete bettings',
        ]);

        $role = Role::create(['name' => 'creator']);
        $role->givePermissionTo([
            'view users',
            'create users',
            'edit users',
            'delete users',

            'view roles',
            'create roles',
            'edit roles',
            'delete roles',
        ]);

        $role = Role::create(['name' => 'member']);
        $role->givePermissionTo([
            'view fights',
            'create fights',
            'edit fights',
            'delete fights',
            
            'view bettings',
            'create bettings',
            'edit bettings',
            'delete bettings',
        ]);
    }
}
