<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBackupBettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backup_bettings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('betting_id')->nullable();
            $table->bigInteger('fight_id')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->string('ring_side'); // wala or meron
            $table->string('amount');
            $table->string('payout')->default(0);
            $table->string('tong')->default(0);
            $table->string('payout_status')->nullable();
            $table->string('reference_number')->nullable();
            $table->string('mashador_bettor_name')->nullable();
            $table->boolean('claimed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backup_bettings');
    }
}
