<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MashadorColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bettings', function (Blueprint $table) {
            $table->string('reference_number')->nullable()->after('payout_status');
            $table->string('mashador_bettor_name')->nullable()->after('reference_number');
            $table->string('mashador_payout')->nullable()->after('mashador_bettor_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bettings', function (Blueprint $table) {
            $table->dropColumn('reference_number');
            $table->dropColumn('mashador_bettor_name');
            $table->dropColumn('mashador_payout');
        });
    }
}
