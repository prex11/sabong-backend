<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBackupFightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backup_fights', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('fight_id')->nullable();
            $table->bigInteger('tournament_id')->nullable();
            $table->string('fight_number')->nullable();
            $table->string('wala_name')->nullable();
            $table->string('wala_picture')->nullable();
            $table->string('meron_name')->nullable();
            $table->string('meron_picture')->nullable();
            $table->string('total_bettings')->default(0)->nullable();
            $table->string('total_fighter_bettings')->default(0)->nullable();
            $table->string('status')->default('pending'); // pending, open, on-going, close, finish
            $table->string('time_to_close')->nullable();
            $table->string('time_to_finish_fight')->nullable();
            $table->boolean('disable_timer_autoclose')->default(0);
            $table->string('total_payouts')->default(0);
            $table->string('total_earnings')->default(0)->nullable();
            $table->string('winner')->nullable(); //wala or meron
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backup_fights');
    }
}
