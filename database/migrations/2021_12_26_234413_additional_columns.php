<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdditionalColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fights', function (Blueprint $table) {
            $table->string('total_payouts')->default(0)->after('status');
        });

        Schema::table('tournaments', function (Blueprint $table) {
            $table->string('tong_percentage')->default(15)->after('description');
        });

        Schema::table('bettings', function (Blueprint $table) {
            $table->string('payout')->default(0)->after('amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fights', function (Blueprint $table) {
            $table->dropColumn('total_payouts');
        });

        Schema::table('tournaments', function (Blueprint $table) {
            $table->dropColumn('tong_percentage');
        });

        Schema::table('bettings', function (Blueprint $table) {
            $table->dropColumn('payout');
        });
    }
}
