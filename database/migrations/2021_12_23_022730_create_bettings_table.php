<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bettings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('fight_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->string('ring_side'); // wala or meron
            $table->string('amount');
            $table->timestamps();

            $table->foreign('fight_id')->references('id')->on('fights')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bettings');
    }
}
