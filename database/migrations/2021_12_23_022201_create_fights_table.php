<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fights', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tournament_id')->unsigned();
            $table->string('fight_number')->nullable();
            $table->string('wala_name')->nullable();
            $table->string('wala_picture')->nullable();
            $table->string('meron_name')->nullable();
            $table->string('meron_picture')->nullable();
            $table->string('total_bettings')->default(0)->nullable();
            $table->string('total_fighter_bettings')->default(0)->nullable();
            $table->string('status')->default('pending'); // pending, open, on-going, close, finish
            $table->timestamps();

            $table->foreign('tournament_id')->references('id')->on('tournaments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fights');
    }
}
