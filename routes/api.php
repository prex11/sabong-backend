<?php

use Illuminate\Support\Facades\Route;
use CloudCreativity\LaravelJsonApi\Facades\JsonApi;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api\V1\Auth')->prefix('api/v1')->middleware('json.api')->group(function () {
    Route::post('/login', 'LoginController');
    Route::post('/register', 'RegisterController');
    Route::post('/logout', 'LogoutController')->middleware('auth:api');
    Route::post('/password-forgot', 'ForgotPasswordController');
    Route::post('/password-reset', 'ResetPasswordController');
    Route::post('/login/mobile', 'LoginController@mobileLogin');
});

Route::namespace('Api\V1')->prefix('api/v1')->middleware('auth:api')->group(function () {
    Route::post('/uploads/{resource}/{id}/{field}', 'UploadController')->where('path', '.*');
    Route::post('/fight-uploads/{resource}/{field}', 'FightUploadController')->where('path', '.*');
});

JsonApi::register('v1')->middleware('auth:api')->routes(function ($api) {
    $api->get('me', 'Api\V1\MeController@readProfile');
    $api->patch('me', 'Api\V1\MeController@updateProfile');

    $api->post('place-bet', 'Api\V1\BettingController@placeBet');
    $api->post('place-bet-mashador', 'Api\V1\BettingController@placeBetMashador');

    $api->patch('fight-update-status/{id}', 'Api\V1\FightController@updateStatus');
    $api->patch('start-timer-to-finish-fight/{id}', 'Api\V1\FightController@startTimerToFinishFight');
    $api->patch('finish-fight/{id}', 'Api\V1\FightController@finishMatch');
    $api->patch('undo-fight/{id}', 'Api\V1\FightController@undoFinishMatch');
    $api->patch('cancel-fight/{id}', 'Api\V1\FightController@cancelMatch');
    $api->get('get-current-fight', 'Api\V1\FightController@getCurrentFight');
    $api->get('get-fights-betted-by-user', 'Api\V1\FightController@getFightsBettedByUser');

    $api->post('add-credit/{id}', 'Api\V1\UserController@addCredit');
    $api->post('cash-out/{id}', 'Api\V1\UserController@cashOut');
    $api->post('add-revolving-fund/{id}', 'Api\V1\UserController@addRevolvingFund');
    $api->post('transfer-credit-to-user', 'Api\V1\UserController@transferCreditToUser');    
    $api->get('get-stations-income', 'Api\V1\UserController@getStationsIncome');
    

    $api->resource('users', [
        'has-one' => 'roles'
    ]);
    $api->resource('roles', [
        'has-many' => 'permissions'
    ]);
    $api->resource('permissions', [
        'has-one' => 'roles'
    ])->only('index');

    $api->resource('tournaments');
    $api->resource('fights');
    $api->resource('bettings');
    $api->resource('credit-histories');
});
